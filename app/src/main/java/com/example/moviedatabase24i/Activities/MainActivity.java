package com.example.moviedatabase24i.Activities;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.moviedatabase24i.CustomClasses.Constants;
import com.example.moviedatabase24i.CustomClasses.Movie;
import com.example.moviedatabase24i.CustomClasses.MovieResults;
import com.example.moviedatabase24i.Other.DeviseSize;
import com.example.moviedatabase24i.Other.JsonPlaceHolderApi;
import com.example.moviedatabase24i.Other.MovieAdapter;
import com.example.moviedatabase24i.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    TextView ageTextView;

    private RecyclerView movieListView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private LinearLayout optionsLayout;
    private Menu optionsMenu;

    private ArrayList<Movie> movies = new ArrayList<>();
    private int tries = 0;
    private int pageNumber = 1;
    private int lastPage = 0;
    private int totalPages = 0;
    private int scrollToNumber = 0;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        optionsMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_show_more:
                optionsLayout.setVisibility(View.VISIBLE);

                MenuItem lessItem = optionsMenu.findItem(R.id.menu_show_less);
                lessItem.setVisible(true);
                item.setVisible(false);

                break;
            case R.id.menu_show_less:
                optionsLayout.setVisibility(View.GONE);

                MenuItem moreItem = optionsMenu.findItem(R.id.menu_show_more);
                moreItem.setVisible(true);
                item.setVisible(false);

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ageTextView = findViewById(R.id.age_tv_main);
        movieListView = findViewById(R.id.movie_list_view);
        movieListView.setHasFixedSize(true);

        optionsLayout = findViewById(R.id.options_layout_main);


        /*
          If the devise is a tablet fix the Screen Orientation to Landscape mode
          Else fix it to Portrait mode.

          When the devise is a tablet, the @movieListView scrolls horizontally,
          Else it scrolls vertically.
         */
        if (DeviseSize.isTablet(this)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            layoutManager = new LinearLayoutManager(this,
                    LinearLayoutManager.HORIZONTAL, false);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            layoutManager = new LinearLayoutManager(this,
                    LinearLayoutManager.VERTICAL, false);
        }

        movieListView.setLayoutManager(layoutManager);

        adapter = new MovieAdapter(movies);
        movieListView.setAdapter(adapter);

        if (getIntent().getExtras() != null){
            scrollToNumber = getIntent().getExtras().getInt(Constants.MOVIE_NUMBER_EXTRA);
        }

        /*
          Text changed listener is necessary to determine whether a number of days has been entered
          When a number has been entered the list needs to be reloaded.
         */
        ageTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                movies.clear();
                adapter.notifyDataSetChanged();
                pageNumber = 1;
                lastPage = 0;
                getMovies(pageNumber);
            }
        });

        /*
          On scroll listener added to determine whether the bottom of the list has been reached
          When the bottom of the list has been reached, more data needs to be loaded.
         */
        movieListView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                // Determining whether the bottom of the list has been reached.
                // When the bottom of the list has been reached, a new set of movies will be loaded
                // from the database.
                if (!recyclerView.canScrollHorizontally(RecyclerView.SCROLL_AXIS_HORIZONTAL)
                && !recyclerView.canScrollVertically(RecyclerView.SCROLL_AXIS_VERTICAL)){
                    createLog("Cannot scroll any further");

                    // To ensure that the number of pages is less or equal than the number of
                    // available pages.
                    if (pageNumber <= totalPages) {
                        pageNumber++;
                        getMovies(pageNumber);
                    }
                }
            }
        });

        getMovies(pageNumber);
    }

    /**
     * Performs the GET request to the database
     * Using the Retrofit API.
     * Downloads the data async.
     * @param pageNum = the page number of the requests that needs to be performed
     */

    private void getMovies(final int pageNum){
        // Getting the result from the "Maximum age in days" text field to determine the maximum
        // age of the movies displayed.
        String earliestMovieDate = null;
        String sorting = Constants.SORT_POPULARITY;

        if (!TextUtils.isEmpty(ageTextView.getText().toString())) {
            int daysToDeduct = Integer.valueOf(ageTextView.getText().toString());

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, daysToDeduct * -1);
            earliestMovieDate = dateFormat.format(c.getTime());
            createLog(earliestMovieDate);
            sorting = Constants.SORT_RELEASE_DATE;
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.DISCOVER_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<MovieResults> call = jsonPlaceHolderApi.getMovieResults(Constants.API_KEY,
                false, pageNum, earliestMovieDate, sorting);

        call.enqueue(new Callback<MovieResults>() {
            @Override
            public void onResponse(Call<MovieResults> call, Response<MovieResults> response) {
                /*
                  response code 200 == success
                  response code 401 == invalid api key
                  response code 404 == invalid request, data could not be found.
                 */

                if (response.code() == 200){
                    tries = 0;
                    MovieResults results = response.body();

                    // Gets the total pages.
                    if (results != null) {
                        totalPages = results.getTotalPages();
                        createLog("Total results: " + totalPages);
                    }

                    // Adds all the movie results in an Arraylist<Movie>
                    // Only when the movies are not yet in the list
                    if (lastPage != pageNum) {
                        movies.addAll(Arrays.asList(results.getResults()));
                        adapter.notifyDataSetChanged();
                    }

                    // To make sure that the List scrolls down to your previous selected Movie
                    if (scrollToNumber > movies.size()){
                        pageNumber++;
                        getMovies(pageNumber);
                    } else {
                        movieListView.scrollToPosition(scrollToNumber);
                        createLog("Number to scroll to: " + scrollToNumber);
                    }

                } else {
                    createLog("Something has gone wrong with error code" + response.code());

                    if (tries < 5) {
                        tries ++;
                        Toast.makeText(MainActivity.this, "Something has gone wrong. We are trying again", Toast.LENGTH_SHORT).show();
                        getMovies(pageNum);
                    } else {
                        Toast.makeText(MainActivity.this, "Made " + tries + " attempts but without success. Something has gone wrong.", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<MovieResults> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                // Makes sure that the app only tris 5 times.
                if (tries < 5){
                    tries ++;
                    getMovies(pageNum);
                }
            }
        });
    }

    private void createLog(String message){
        Log.d("Main Activity", message);
    }
}
