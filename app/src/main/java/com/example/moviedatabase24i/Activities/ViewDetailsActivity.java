package com.example.moviedatabase24i.Activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.moviedatabase24i.CustomClasses.Constants;
import com.example.moviedatabase24i.CustomClasses.Movie;
import com.example.moviedatabase24i.CustomClasses.MovieGenre;
import com.example.moviedatabase24i.CustomClasses.MovieGenreList;
import com.example.moviedatabase24i.Other.DeviseSize;
import com.example.moviedatabase24i.Other.JsonPlaceHolderApi;
import com.example.moviedatabase24i.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/*
    This activity is only used to display the Movie details from the selected movie in the
    Main Activity. The activity has no further functionality or function. When the back button
    is clicked, the activity will go back to the main activity.
 */

public class ViewDetailsActivity extends AppCompatActivity {

    private static final String TAG = "ViewDetailsActivity";

    private ImageView movieImageView;
    private TextView movieTextView;

    private List<MovieGenre> genres;

    private Movie currentMovie = null;
    private int movieNumber = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_details);
         /*
          If the devise is a tablet fix the Screen Orientation to Landscape mode
          Else fix it to Portrait mode.
         */
        if (DeviseSize.isTablet(this)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        getMovieGenres();

        movieImageView = findViewById(R.id.detail_image_view);
        movieTextView = findViewById(R.id.detail_text_view);

        //Getting the movie details from the Main Activity.
        if (!getIntent().getExtras().isEmpty()){
            currentMovie = (Movie) getIntent().getExtras().
                    getSerializable(Constants.MOVIE_DETAILS_EXTRA);
            movieNumber = getIntent().getExtras().getInt(Constants.MOVIE_NUMBER_EXTRA);
            setTitle(currentMovie.getTitle());
        }

    }

    private void getMovieGenres() {
        Log.d(TAG, "getMovieGenres has started");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.GENRE_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<MovieGenreList> call = jsonPlaceHolderApi.getMovieGenres(Constants.API_KEY);

        call.enqueue(new Callback<MovieGenreList>() {
            @Override
            public void onResponse(Call<MovieGenreList> call, Response<MovieGenreList> response) {
                Log.d(TAG, "onResponse: response code " + response.code());
                if (response.code() == 200){
                    MovieGenreList list = response.body();
                    genres = list.getMovieGenres();
                    setInfoInView(currentMovie);
                }
            }

            @Override
            public void onFailure(Call<MovieGenreList> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    private void setInfoInView(Movie currentMovie) {
        // Making sure that the text is at the right position compare to the picture of the movie.
        RelativeLayout.LayoutParams layoutParams =
                (RelativeLayout.LayoutParams) movieTextView.getLayoutParams();
        if (DeviseSize.isTablet(this)) {
            layoutParams.addRule(RelativeLayout.RIGHT_OF, R.id.detail_image_view);
            Glide.with(this)
                    .load(Uri.parse(Constants.IMAGE_BASE_URL + currentMovie.getPosterPath()))
                    .into(movieImageView);

            movieTextView.setLineSpacing(0, 1.5f);
        } else {
            layoutParams.addRule(RelativeLayout.BELOW, R.id.detail_image_view);
            Glide.with(this)
                    .load(Uri.parse(Constants.IMAGE_BASE_URL + currentMovie.getBackdropPath()))
                    .into(movieImageView);
        }

        StringBuilder sb = new StringBuilder();
        sb.append("Title: \t").append(currentMovie.getTitle()).append("\n");
        sb.append("Language: \t").append(currentMovie.getOriginalLanguage()).append("\n");
        sb.append("Genre: \t");

        for (int id: currentMovie.getGenreIds()){
            for (MovieGenre genre: genres){
                if (id == genre.getId()){
                    sb.append(genre.getGenreName() + ", ");
                }
            }
        }
        sb.append("\nOverview: \t").append(currentMovie.getOverview()).append("\n");
        sb.append("Release Date: \t").append(currentMovie.getReleaseDate()).append("\n");

        movieTextView.setText(sb);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Constants.MOVIE_NUMBER_EXTRA, movieNumber);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra(Constants.MOVIE_NUMBER_EXTRA, movieNumber);
            startActivity(intent);
        }
        return true;
    }
}
