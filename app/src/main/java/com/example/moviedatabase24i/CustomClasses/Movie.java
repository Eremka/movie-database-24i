package com.example.moviedatabase24i.CustomClasses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Arrays;

public class Movie implements Serializable {
    @SerializedName("vote_count")
    private int voteCount;

    @SerializedName("id")
    private int id;

    @SerializedName("video")
    private boolean video;

    @SerializedName("vote_average")
    private double voteAverage;

    @SerializedName("title")
    private String title;

    @SerializedName("popularity")
    private double popularity;

    @SerializedName("poster_path")
    private String posterPath;

    @SerializedName("original_language")
    private String originalLanguage;

    @SerializedName("original_title")
    private
    String originalTitle;

    @SerializedName("genre_ids")
    private
    int[] genreIds;

    @SerializedName("backdrop_path")
    private String backdropPath;

    @SerializedName("adult")
    private boolean adult;

    @SerializedName("overview")
    private String overview;

    @SerializedName("release_date")
    private String releaseDate;

    public int getVoteCount() {
        return voteCount;
    }

    public int getId() {
        return id;
    }

    public boolean isVideo() {
        return video;
    }

    public double getVoteAverage() {
        return voteAverage;
    }

    public String getTitle() {
        return title;
    }

    public double getPopularity() {
        return popularity;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public int[] getGenreIds() {
        return genreIds;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public boolean isAdult() {
        return adult;
    }

    public String getOverview() {
        return overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "voteCount=" + voteCount + "\n" +
                ", id=" + id + "\n" +
                ", video=" + video + "\n" +
                ", voteAverage=" + voteAverage + "\n" +
                ", title='" + title + "\n" +
                ", popularity=" + popularity + "\n" +
                ", posterPath='" + posterPath + "\n" +
                ", originalLanguage='" + originalLanguage + "\n" +
                ", originalTitle='" + originalTitle + "\n" +
                ", genreIds=" + Arrays.toString(genreIds) + "\n" +
                ", backdropPath='" + backdropPath + "\n" +
                ", adult=" + adult + "\n" +
                ", overview='" + overview + "\n" +
                ", releaseDate='" + releaseDate + "\n" +
                '}';
    }
}
