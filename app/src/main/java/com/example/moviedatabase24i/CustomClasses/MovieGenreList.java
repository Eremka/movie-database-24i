package com.example.moviedatabase24i.CustomClasses;

import com.example.moviedatabase24i.CustomClasses.MovieGenre;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieGenreList {
    @SerializedName("genres")
    private List<MovieGenre> movieGenres;

    public List<MovieGenre> getMovieGenres() {
        return movieGenres;
    }
}
