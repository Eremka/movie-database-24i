package com.example.moviedatabase24i.CustomClasses;

import com.google.gson.annotations.SerializedName;

public class MovieResults {
    @SerializedName("page")
    private int page;

    @SerializedName("total_results")
    private int totalResults;

    @SerializedName("total_pages")
    private int totalPages;

    @SerializedName("results")
    private Movie[] results;

    public int getPage() {
        return page;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public Movie[] getResults() {
        return results;
    }
}
