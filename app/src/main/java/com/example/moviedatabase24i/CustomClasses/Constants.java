package com.example.moviedatabase24i.CustomClasses;

public class Constants {

    public static final String API_KEY = "18d4af72c19b9f61de07fee898aedb83";
    public static final String DISCOVER_BASE_URL = "https://api.themoviedb.org/3/discover/";
    public static final String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w500";
    public static final String GENRE_BASE_URL = "https://api.themoviedb.org/3/genre/movie/";

    public static final String SORT_RELEASE_DATE = "release_date.asc";
    public static final String SORT_POPULARITY = "popularity.desc";

    public static final String MOVIE_DETAILS_EXTRA = "movie_details";
    public static final String MOVIE_NUMBER_EXTRA = "movie_number";
}
