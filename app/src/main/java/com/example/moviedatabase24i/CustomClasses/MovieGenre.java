package com.example.moviedatabase24i.CustomClasses;

import com.google.gson.annotations.SerializedName;

public class MovieGenre {
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String genreName;

    public int getId() {
        return id;
    }

    public String getGenreName() {
        return genreName;
    }
}
