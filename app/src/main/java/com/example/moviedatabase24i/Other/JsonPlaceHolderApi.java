package com.example.moviedatabase24i.Other;

import com.example.moviedatabase24i.CustomClasses.MovieGenreList;
import com.example.moviedatabase24i.CustomClasses.MovieResults;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface JsonPlaceHolderApi {

    @GET("movie")
    Call<MovieResults> getMovieResults(
            @Query("api_key") String apiKey,
            @Query("include_adult") boolean incAdult,
            @Query("page") int page,
            @Query("release_date.gte") String releaseDate,
            @Query("sort_by") String sort
    );

    @GET("list")
    Call<MovieGenreList> getMovieGenres(
            @Query("api_key") String apiKey
            );
}
