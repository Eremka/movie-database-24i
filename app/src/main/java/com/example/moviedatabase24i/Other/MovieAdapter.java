package com.example.moviedatabase24i.Other;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.moviedatabase24i.Activities.ViewDetailsActivity;
import com.example.moviedatabase24i.CustomClasses.Constants;
import com.example.moviedatabase24i.CustomClasses.Movie;
import com.example.moviedatabase24i.R;

import java.util.List;


public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {

    private List<Movie> movieList;

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public View listItemView;
        public ViewHolder(View itemView) {
            super(itemView);
            listItemView = itemView;
        }
    }

    public MovieAdapter(List<Movie> list){
        this.movieList = list;
    }

    @NonNull
    @Override
    public MovieAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MovieAdapter.ViewHolder holder, final int position) {
        Movie currentMovie = movieList.get(position);

        ImageView pictureView = holder.listItemView.findViewById(R.id.image_list_item);
        TextView titleView = holder.listItemView.findViewById(R.id.title_tv_list_item);

        if (DeviseSize.isTablet(holder.listItemView.getContext())){
            int widthPixels = (int) (Resources.getSystem().getDisplayMetrics().widthPixels / 2.4);
            pictureView.setMaxWidth(widthPixels);
            pictureView.setMinimumWidth(widthPixels);

            Glide.with(holder.listItemView.getContext())
                    .load(Uri.parse(Constants.IMAGE_BASE_URL + currentMovie.getPosterPath()))
                    .into(pictureView);
        } else {
            Glide.with(holder.listItemView.getContext())
                    .load(Uri.parse(Constants.IMAGE_BASE_URL + currentMovie.getBackdropPath()))
                    .into(pictureView);
        }

        titleView.setText(currentMovie.getTitle());

        // Dealing with the on item click
        holder.listItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.listItemView.getContext(), ViewDetailsActivity.class);
                intent.putExtra(Constants.MOVIE_DETAILS_EXTRA, movieList.get(position));
                intent.putExtra(Constants.MOVIE_NUMBER_EXTRA, position);
                holder.listItemView.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    private void createLog(String message){
        Log.d("Movie Adapter", message);
    }
}
