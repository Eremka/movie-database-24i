package com.example.moviedatabase24i.Other;

import android.content.Context;
import android.content.res.Configuration;

public class DeviseSize {
    /**
     * Method to determine whether the devise is a tablet or a phone.
     * @return a boolean value when the devise is a tablet
     */

    public static boolean isTablet(Context context){
        return ((context.getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE);
    }
}
